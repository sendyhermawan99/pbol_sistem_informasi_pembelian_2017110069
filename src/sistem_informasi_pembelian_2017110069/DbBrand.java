/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistem_informasi_pembelian_2017110069;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

/**
 *
 * @author SENARIYUS
 */
public class DbBrand {

    int KdBrand;
    String NamaBrand;

    public int getKdBrand() {
        return KdBrand;
    }

    public void setKdBrand(int KdBrand) {
        this.KdBrand = KdBrand;
    }

    public String getNamaBrand() {
        return NamaBrand;
    }

    public void setNamaBrand(String NamaBrand) {
        this.NamaBrand = NamaBrand;
    }
            
    
    
          public boolean insert(){
        boolean berhasil=false;
        Koneksi con=new Koneksi();
        try {
            con.bukaKoneksi();
            con.preparedStatement = con.dbkoneksi.prepareStatement
        ("Insert into brand"+"(Kd_Brand, Nama_Brand) values (?,?)");
                     con.preparedStatement.setInt(1, this.KdBrand);
                     con.preparedStatement.setString(2, this.NamaBrand);
                     con.preparedStatement.executeUpdate();
         berhasil=true;
        } catch(Exception e){
            e.printStackTrace();
            berhasil=false;
        } finally {
          con.tutupKoneksi();  
          return berhasil;          
        }
    }

    
          
              public Vector Load(){
        try{
            Vector tableData = new Vector();
            Koneksi con = new Koneksi();
            con.bukaKoneksi();
            con.statement = con.dbkoneksi.createStatement();
            ResultSet rs = con.statement.executeQuery
        ("Select Kd_Brand, Nama_Brand from brand");
        int i=1; 
        while (rs.next()){
            Vector <Object> row = new Vector <Object>();
            row.add(i);
            row.add(rs.getInt("Kd_Brand"));
            row.add(rs.getString("Nama_Brand"));
            tableData.add(row);
            i++;
        }
        con.tutupKoneksi();
        return tableData;
        }catch(SQLException ex){
            ex.printStackTrace();
            return null;
        } 
}

public boolean delete(Integer KdBrand, String NamaBarang){
    boolean berhasil = false;
    Koneksi con = new Koneksi();
    try{
        con.bukaKoneksi();
        con.preparedStatement = con.dbkoneksi.prepareStatement
        ("delete from barang where Kd_Brand"+" = ? Nama_Brand"+"=?" );
        con.preparedStatement.setInt(1, KdBrand);
        con.preparedStatement.setString(2, NamaBarang);
        con.preparedStatement.executeUpdate();
           berhasil = true;
        }catch (Exception e){
            e.printStackTrace();
            berhasil = false;
        } finally{
        con.tutupKoneksi();
        return berhasil;
    }
}

              
 public boolean select(int nomor){
     try{
         Koneksi con = new Koneksi();
         con.bukaKoneksi();
         con.statement = con.dbkoneksi.createStatement();
         ResultSet rs = con.statement.executeQuery
        ("select Kd_Brand, Nama_Brand "+" from brand where Kd_Brand = "+nomor);
             while (rs.next()){
             this.KdBrand = rs.getInt("Kd_Brand");
             this.NamaBrand = rs.getString("Nama_Brand");
             }
         con.tutupKoneksi();
         return true;
       } catch (SQLException ex){
           ex.printStackTrace();
           return false;
       }
 }
    
 public int validasiBarang(String NamaBarang){
        int val = 0;
        try{
            Koneksi con = new Koneksi();
            con.bukaKoneksi();
            con.statement = con.dbkoneksi.createStatement();
           ResultSet rs = con.statement.executeQuery
        ("Select count(*) as jml "+"from brand where Nama_BrandS = '"+NamaBrand+"'");
            while (rs.next()){
                val = rs.getInt("jml");
        }
        con.tutupKoneksi();
        }catch (SQLException ex){
         ex.printStackTrace();
        }   
        return val;
        }


 public Vector Lookup(String fld, String dt){
     
    try{
        Vector tableData =  new Vector();
        Koneksi con = new Koneksi();
        con.bukaKoneksi();
        con.statement = con.dbkoneksi.createStatement();
        ResultSet rs = con.statement.executeQuery
        ("Select Kd_Brand, Soh from barang where "+fld+ " like '%"+dt+"%'");
        int i=1;
        while (rs.next()){
            Vector<Object> row = new Vector<Object>();
            row.add(i);
            row.add(rs.getInt("Kd_Brand"));
            row.add(rs.getString("Nama_Brand"));
            tableData.add(row);
            i++;
        }
        con.tutupKoneksi(); return tableData;
    }catch (SQLException ex){
        ex.printStackTrace();   return null;} 
 }

public boolean update(int id){
        boolean berhasil = false;
        Koneksi con = new Koneksi();
        try{
            con.bukaKoneksi();
            con.preparedStatement = con.dbkoneksi.prepareStatement
        ("update barang set Nama_Brand=? "+" ,Nama_Brand=? where Kd_Brand = ?");
                     con.preparedStatement.setString(1, NamaBrand);
                     con.preparedStatement.setInt(4,KdBrand );
                     con.preparedStatement.setInt(7, KdBrand);
                     con.preparedStatement.executeUpdate();
                    // con.preparedStatement.executeUpdate();
                     
                     
             
                     berhasil = true;
                     
        } catch (Exception e){
            e.printStackTrace();
            berhasil = false;
        } finally {
            con.tutupKoneksi();
            return berhasil;
        }
    }

}



