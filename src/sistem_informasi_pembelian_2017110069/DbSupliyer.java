/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistem_informasi_pembelian_2017110069;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

/**
 *
 * @author SENARIYUS
 */
public class DbSupliyer {
    
    int KdSupliyer;
    String NamaSupliyer;
    String Hp;
    String Alamat;

    public int getKdSupliyer() {
        return KdSupliyer;
    }

    public void setKdSupliyer(int KdSupliyer) {
        this.KdSupliyer = KdSupliyer;
    }

    public String getNamaSupliyer() {
        return NamaSupliyer;
    }

    public void setNamaSupliyer(String NamaSupliyer) {
        this.NamaSupliyer = NamaSupliyer;
    }

    public String getHp() {
        return Hp;
    }

    public void setHp(String Hp) {
        this.Hp = Hp;
    }

    public String getAlamat() {
        return Alamat;
    }

    public void setAlamat(String Alamat) {
        this.Alamat = Alamat;
    }
    
    
          public boolean insert(){
        boolean berhasil=false;
        Koneksi con=new Koneksi();
        try {
            con.bukaKoneksi();
            con.preparedStatement = con.dbkoneksi.prepareStatement
        ("Insert into supliyer"+"(Kd_Supliyer, Nama_Supliyer, Hp, Alamat) values (?,?,?,?)");
                     con.preparedStatement.setInt(1, this.KdSupliyer);
                     con.preparedStatement.setString(2, this.NamaSupliyer);
                     con.preparedStatement.setString(3, this.Hp);
                     con.preparedStatement.setString(4, this.Alamat);
                     con.preparedStatement.executeUpdate();
         berhasil=true;
        } catch(Exception e){
            e.printStackTrace();
            berhasil=false;
        } finally {
          con.tutupKoneksi();  
          return berhasil;          
        }
    }
    
    public Vector Load(){
        try{
            Vector tableData = new Vector();
            Koneksi con = new Koneksi();
            con.bukaKoneksi();
            con.statement = con.dbkoneksi.createStatement();
            ResultSet rs = con.statement.executeQuery
        ("Select Kd_Supliyer, Nama_Supliyer, Hp, Alamat from supliyer");
        int i=1; 
        while (rs.next()){
            Vector <Object> row = new Vector <Object>();
            row.add(i);
            row.add(rs.getInt("Kd_Supliyer"));
            row.add(rs.getString("Nama_Supliyer"));
            row.add(rs.getString("Hp"));
            row.add(rs.getString("Alamat"));
            tableData.add(row);
            i++;
        }
        con.tutupKoneksi();
        return tableData;
        }catch(SQLException ex){
            ex.printStackTrace();
            return null;
        } 
}

    public boolean delete(Integer KdSupliyer, String NamaSupliyer, String Hp, String Alamat){
    boolean berhasil = false;
    Koneksi con = new Koneksi();
    try{
        con.bukaKoneksi();
        con.preparedStatement = con.dbkoneksi.prepareStatement
        ("delete from barang where Kd_Supliyer"+" = ? Nama_Supliyer"+" = ? Hp"+" = ? and Alamat = ?" );
        con.preparedStatement.setInt(1, KdSupliyer);
        con.preparedStatement.setString(2, NamaSupliyer);
        con.preparedStatement.setString(3, Hp);
        con.preparedStatement.setString(4, Alamat);
        con.preparedStatement.executeUpdate();
           berhasil = true;
        }catch (Exception e){
            e.printStackTrace();
            berhasil = false;
        } finally{
        con.tutupKoneksi();
        return berhasil;
    }
}

     public boolean select(int nomor){
     try{
         Koneksi con = new Koneksi();
         con.bukaKoneksi();
         con.statement = con.dbkoneksi.createStatement();
         ResultSet rs = con.statement.executeQuery
        ("select Kd_Supliyer, Nama_Supliyer, Hp, Alamat "+" from barang where Kd_Supliyer = "+nomor);
             while (rs.next()){
             this.KdSupliyer = rs.getInt("Kd_Suplier");
             this.NamaSupliyer = rs.getString("Nama_Supliyer");
             this.Hp = rs.getString("Hp");
             this.Alamat = rs.getString("Alamat");
             }
         con.tutupKoneksi();
         return true;
       } catch (SQLException ex){
           ex.printStackTrace();
           return false;
       }
 }

     public int validasiBarang(String NamaSupliyer){
        int val = 0;
        try{
            Koneksi con = new Koneksi();
            con.bukaKoneksi();
            con.statement = con.dbkoneksi.createStatement();
           ResultSet rs = con.statement.executeQuery
        ("Select count(*) as jml "+"from Supliyer where Nama_Supliyer = '"+NamaSupliyer+"'");
            while (rs.next()){
                val = rs.getInt("jml");
        }
        con.tutupKoneksi();
        }catch (SQLException ex){
         ex.printStackTrace();
        }   
        return val;
        }

    public Vector Lookup(String fld, String dt){
     
    try{
        Vector tableData =  new Vector();
        Koneksi con = new Koneksi();
        con.bukaKoneksi();
        con.statement = con.dbkoneksi.createStatement();
        ResultSet rs = con.statement.executeQuery
        ("Select Kd_Supliyer, Nama_Supliyer, Hp, Alamat from supliyer where "+fld+ " like '%"+dt+"%'");
        int i=1;
        while (rs.next()){
            Vector<Object> row = new Vector<Object>();
            row.add(i);
            row.add(rs.getInt("Kd_Supliyer"));
            row.add(rs.getString("Nama_Supliyer"));
            row.add(rs.getString("Hp"));
            row.add(rs.getString("Alamat"));
            tableData.add(row);
            i++;
        }
        con.tutupKoneksi(); return tableData;
    }catch (SQLException ex){
        ex.printStackTrace();   return null;} 
 }

public boolean update(int id){
        boolean berhasil = false;
        Koneksi con = new Koneksi();
        try{
            con.bukaKoneksi();
            con.preparedStatement = con.dbkoneksi.prepareStatement
        ("update supliyer set Nama_Supliyer=? "+" , Hp=? "+" , Alamat=? where Kd_Supliyer = ?");
                     con.preparedStatement.setString(1, NamaSupliyer);
                     con.preparedStatement.setString(2, Hp);
                     con.preparedStatement.setString(3, Alamat);
                     con.preparedStatement.executeUpdate();
                     berhasil = true;
                     
        } catch (Exception e){
            e.printStackTrace();
            berhasil = false;
        } finally {
            con.tutupKoneksi();
            return berhasil;
        }
    }


     
     
}
