/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistem_informasi_pembelian_2017110069;

import java.sql.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

/**
 *
 * @author SENARIYUS
 */
public class Koneksi {
    
        public Connection dbkoneksi;
    public Statement statement;
    public PreparedStatement preparedStatement;
        
        public Koneksi(){
            this.dbkoneksi = null;
        }
     
        public void bukaKoneksi(){
            try {
                Class.forName("com.mysql.jdbc.Driver");
            dbkoneksi = DriverManager.getConnection("jdbc:mysql://localhost:3306/sistem_penjualan_2017110069?user=root");
            } catch (Exception e){
                e.printStackTrace();
            }
        }
            public void tutupKoneksi(){
                try {
                      if(statement != null)       statement.close();
                    if(preparedStatement != null)  preparedStatement.close();
                    if(dbkoneksi != null)   dbkoneksi.close();
                }   catch (Exception e){
                    throw new RuntimeException(e.getMessage());
                }
            }

    
}
