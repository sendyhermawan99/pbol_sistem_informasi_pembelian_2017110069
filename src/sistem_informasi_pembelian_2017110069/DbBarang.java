/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistem_informasi_pembelian_2017110069;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

/**
 *
 * @author SENARIYUS
 */
public class DbBarang {

    int KdBarang;
    String NamaBarang;
    String Status;
    String Kategori;
    int KdBrand;
    int Soh;
    int Harga;

    public int getKdBarang() {
        return KdBarang;
    }

    public void setKdBarang(int KdBarang) {
        this.KdBarang = KdBarang;
    }

    public String getNamaBarang() {
        return NamaBarang;
    }

    public void setNamaBarang(String NamaBarang) {
        this.NamaBarang = NamaBarang;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getKategori() {
        return Kategori;
    }

    public void setKategori(String Kategori) {
        this.Kategori = Kategori;
    }

    public int getKd_Barang() {
        return KdBrand;
    }

    public void setKd_Barang(int Kd_Barang) {
        this.KdBrand = Kd_Barang;
    }

    public int getSoh() {
        return Soh;
    }

    public void setSoh(int Soh) {
        this.Soh = Soh;
    }

    public int getHarga() {
        return Harga;
    }

    public void setHarga(int Harga) {
        this.Harga = Harga;
    }

    
    
      public boolean insert(){
        boolean berhasil=false;
        Koneksi con=new Koneksi();
        try {
            con.bukaKoneksi();
            con.preparedStatement = con.dbkoneksi.prepareStatement("Insert into barang"+
                    "(Kd_Barang, Nama_Barang, Status, Kategori, Kd_Brand, Soh, Harga) values (?,?,?,?,?,?,?)");
                     con.preparedStatement.setInt(1, this.KdBarang);
                     con.preparedStatement.setString(2, this.NamaBarang);
                     con.preparedStatement.setString(3, this.Status);
                     con.preparedStatement.setString(4, this.Kategori);
                     con.preparedStatement.setInt(5, this.KdBrand);
                     con.preparedStatement.setInt(6, this.Soh);
                     con.preparedStatement.setInt(7, this.Harga);
                     con.preparedStatement.executeUpdate();
         berhasil=true;
        } catch(Exception e){
            e.printStackTrace();
            berhasil=false;
        } finally {
          con.tutupKoneksi();  
          return berhasil;          
        }
    }
    
    public Vector Load(){
        try{
            Vector tableData = new Vector();
            Koneksi con = new Koneksi();
            con.bukaKoneksi();
            con.statement = con.dbkoneksi.createStatement();
            ResultSet rs = con.statement.executeQuery
        ("Select Kd_Barang, Nama_Barang, Status, Kategori, Kd_Brand, Soh, Harga from barang");
        int i=1; 
        while (rs.next()){
            Vector <Object> row = new Vector <Object>();
            row.add(i);
            row.add(rs.getInt("Kd_Barang"));
            row.add(rs.getString("Nama_Barang"));
            row.add(rs.getString("Status"));
            row.add(rs.getString("Kategori"));
            row.add(rs.getInt("Kd_Brand"));
            row.add(rs.getInt("Soh"));
            row.add(rs.getInt("Harga"));
            tableData.add(row);
            i++;
        }
        con.tutupKoneksi();
        return tableData;
        }catch(SQLException ex){
            ex.printStackTrace();
            return null;
        } 
}

public boolean delete(Integer KdBarang, String NamaBarang, String Status, String Kategori,Integer KdBrand, Integer Soh, Integer Harga){
    boolean berhasil = false;
    Koneksi con = new Koneksi();
    try{
        con.bukaKoneksi();
        con.preparedStatement = con.dbkoneksi.prepareStatement
        ("delete from barang where Kd_Barang"+" = ? Nama_Barang"+" = ? Status"+" = ? Kategori"+" = ? Kd_Brand"+"= ? Soh"+"= ? and Harga = ?" );
        con.preparedStatement.setInt(1, KdBarang);
        con.preparedStatement.setString(2, NamaBarang);
        con.preparedStatement.setString(3, Status);
        con.preparedStatement.setString(4, Kategori);
        con.preparedStatement.setInt(5, KdBrand);
        con.preparedStatement.setInt(6, Soh);
        con.preparedStatement.setInt(7, Harga);
        con.preparedStatement.executeUpdate();
           berhasil = true;
        }catch (Exception e){
            e.printStackTrace();
            berhasil = false;
        } finally{
        con.tutupKoneksi();
        return berhasil;
    }
}

 public boolean select(int nomor){
     try{
         Koneksi con = new Koneksi();
         con.bukaKoneksi();
         con.statement = con.dbkoneksi.createStatement();
         ResultSet rs = con.statement.executeQuery
        ("select Kd_Barang, Nama_Barang, Status, Kategori, Kd_Brand, Soh, Harga "+" from barang where Kd_Barang = "+nomor);
             while (rs.next()){
             this.KdBarang = rs.getInt("id_barang");
             this.NamaBarang = rs.getString("Nama_Barang");
             this.Status = rs.getString("Status");
             this.Kategori = rs.getString("Kategori");
             this.KdBrand = rs.getInt("Kd_Brand");
             this.Soh = rs.getInt("Soh");
             this.Harga = rs.getInt("Harga");
             }
         con.tutupKoneksi();
         return true;
       } catch (SQLException ex){
           ex.printStackTrace();
           return false;
       }
 }
 
public int validasiBarang(String NamaBarang){
        int val = 0;
        try{
            Koneksi con = new Koneksi();
            con.bukaKoneksi();
            con.statement = con.dbkoneksi.createStatement();
           ResultSet rs = con.statement.executeQuery
        ("Select count(*) as jml "+"from barang where Nama_Barang = '"+NamaBarang+"'");
            while (rs.next()){
                val = rs.getInt("jml");
        }
        con.tutupKoneksi();
        }catch (SQLException ex){
         ex.printStackTrace();
        }   
        return val;
        }

public Vector Lookup(String fld, String dt){
     
    try{
        Vector tableData =  new Vector();
        Koneksi con = new Koneksi();
        con.bukaKoneksi();
        con.statement = con.dbkoneksi.createStatement();
        ResultSet rs = con.statement.executeQuery
        ("Select Kd_Barang, Nama_Barang, Status, Kategori, Kd_Brand, Soh, Harga from barang where "+fld+ " like '%"+dt+"%'");
        int i=1;
        while (rs.next()){
            Vector<Object> row = new Vector<Object>();
            row.add(i);
            row.add(rs.getInt("Kd_Barang"));
            row.add(rs.getString("Nama_Barang"));
            row.add(rs.getString("Status"));
            row.add(rs.getString("Kategori"));
            row.add(rs.getInt("Kd_Brand"));
            row.add(rs.getInt("Soh"));
            row.add(rs.getInt("Harga"));
            tableData.add(row);
            i++;
        }
        con.tutupKoneksi(); return tableData;
    }catch (SQLException ex){
        ex.printStackTrace();   return null;} 
 }

public boolean update(int id){
        boolean berhasil = false;
        Koneksi con = new Koneksi();
        try{
            con.bukaKoneksi();
            con.preparedStatement = con.dbkoneksi.prepareStatement
        ("update barang set Nama_Barang=? "+" , Status=? "+" , Kategori=? "+" , Kd_Brand=? "+" , Soh=? "+" , Harga=? where Kd_Barang = ?");
                     con.preparedStatement.setString(1, NamaBarang);
                     con.preparedStatement.setString(2, Status);
                     con.preparedStatement.setString(3, Kategori);
                     con.preparedStatement.setInt(4,KdBrand );
                     con.preparedStatement.setInt(5,Soh );
                     con.preparedStatement.setInt(6, Harga);
                     con.preparedStatement.setInt(7, KdBarang);
                     con.preparedStatement.executeUpdate();
                    // con.preparedStatement.executeUpdate();
             
                     berhasil = true;
                     
        } catch (Exception e){
            e.printStackTrace();
            berhasil = false;
        } finally {
            con.tutupKoneksi();
            return berhasil;
        }
    }

    
    
}
